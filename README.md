# Counter App using Flutter Riverpod

A simple counter app that uses Flutter Riverpod for state management

## Flutter Riverpod Package

[https://pub.dev/packages/flutter_riverpod](https://pub.dev/packages/flutter_riverpod)

[![Alt text](https://groundgurus-assets.s3.ap-southeast-1.amazonaws.com/Ground+Gurus+-+Logo+Small.png)](https://www.facebook.com/groups/1290693181003142)
